<?php

namespace Scandinaver\Classes;

/**
 * Created by PhpStorm.
 * User: whiskey
 * Date: 29.11.14
 * Time: 3:40
 *
 * класс  для работы с переменными внутри View
 */
class Data {

    protected $seotitle;
    protected $description;
    protected $keywords;

    public function setData($name, $value){
        $this->$name = $value;
    }

    public function destroy($name){
        unset($this->$name);
    }

    public function getData($name){
        return $this->$name;
    }

    public function __get($name)
    {
        if(isset($this->$name))
            return $this->$name;
    }
} 