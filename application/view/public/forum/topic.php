<?php
use Carbon\Carbon;
use Scandinaver\Classes\User;
?>
<section id="topic" class="container" style="margin-top: 50px;margin-bottom: 50px;">
    <header class="row forum-head">
        <div class="col-md-12">
            <div class="row">
                <?php //d($this->data->topic->name);?>
                <?php //d($this->data->posts)?>
                <h3 class="no-margin"><?= $this->data->topic->name ?></h3>
            </div>
            <div class="row">
                <div class="breadcrumbs pull-left col-lg-7 col-md-8 col-sm-6 hidden-xs">
                    <ul class="breadcrumb">
                        <li>
                            <a href="/forum">Форум</a>
                        </li>
                        <li>
                            <a href="/forum/<?= $this->data->topic->forum_id ?>"><?= $this->data->topic->forum->name ?></a>
                        </li>
                        <li>
                            <?= $this->data->topic->name ?>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-5 col-md-4 col-sm-6 col-xs-12 pull-right text-right">
                    <ul class="pagination pull-right">
                        <?php echo $this->data->pager->first(' <li class="page-start"><a href="?page={nr}">В начало</a></li> '); ?>
                        <?php echo $this->data->pager->numbers(' <li><a href="?page={nr}">{nr}</a></li>', ' <li class="active"><a href="javascript:void(0)">{nr}</a></li>'); ?>
                        <?php echo $this->data->pager->last(' <li class="page-end"><a href="?page={nr}">В конец</a></li> '); ?>
                    </ul>
                </div>
            </div>
        </div>
    </header>
    <div class="row">
        <div class="forums-wrapper col-lg-12">
            <div class="row topic topic-description forum-section">
                <div class="col-lg-2 col-md-2 col-sm-2 hidden-xs author-info">
                    <div class="content">
                        <div class="text-center">
                            <span><?= $this->data->topic->author->name ?></span>
                            <div class="avatar-wrapper">
                                <div class="avatar"
                                     style="background-image: url(<?= $this->data->topic->author->avatar ?>)">
                                </div>
                            </div>
                            <small>Зарегестрирован: <br><?= $this->data->topic->author->created_at->diffInDays(Carbon::now())?> дней назад</small>
                        </div>
                    </div>
                </div>
                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                    <div class="content">
                        <p class="date">
                            <i class="ion-ios-clock-outline"></i>
                            &nbsp;&nbsp;<?= $this->data->topic->created_at->format('Y.m.d H:m') ?>

                            <span class="pull-right">
                                <i class="ion-search"></i>
                                    <?= $this->data->topic->view ?>
                                <i class="ion-chatboxes"></i>
                                    <?= $this->data->total ?>
                            </span>
                        </p>
                        <p><?= $this->data->topic->description ?></p>
                        <span class="autor-info text-right hidden-lg hidden-md hidden-sm col-xs-12">
                            <?= $this->data->topic->author->name ?>
                        </span>
                    </div>
                </div>
            </div>
            <hr/>
            <div class="row forum-section">
                <div class="col-md-12">
                    <?php foreach ($this->data->topic->posts as $post): ?>
                        <div class="row post" id="<?= $post->id?>">
                            <div class="col-lg-2 col-md-2 col-sm-2 hidden-xs author-info">
                                <div class="text-center">
                                    <span><?= $post->author->name ?></span>
                                    <div class="avatar-wrapper">
                                        <div class="avatar"
                                             style="background-image: url(<?= $post->author->avatar ?>)">
                                        </div>
                                    </div>
                                    <small><?= $post->author->posts->count() ?>&nbsp;ответов</small>
                                    <small>Зарегестрирован: <br><?= $post->author->created_at->diffInDays(Carbon::now())?> дней назад</small>
                                </div>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <div class="content">
                                    <p class="date">
                                        <i class="ion-ios-clock-outline"></i>
                                        &nbsp;&nbsp;<?= $post->created_at->format('Y.m.d H:m') ?>
                                    </p>
                                    <p><?= $post->text ?></p>
                                    <span class="autor-info text-right hidden-lg hidden-md hidden-sm col-xs-12">
                                    <?= $this->data->topic->author->name ?>
                                </span>
                                </div>
                                <?php if (User::$_admin): ?>
                                    <div class="forum-control">
                                        <i style="font-size: 30px;" class="ion-ios-close-empty pointer post-delete" data-id="<?= $post->id ?>"></i>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 text-right">
                    <ul class="pagination">
                        <?php echo $this->data->pager->first(' <li class="page-start"><a href="?page={nr}">В начало</a></li> '); ?>
                        <?php echo $this->data->pager->numbers(' <li><a href="?page={nr}">{nr}</a></li>', ' <li class="active"><a href="javascript:void(0)">{nr}</a></li>'); ?>
                        <?php echo $this->data->pager->last(' <li class="page-end"><a href="?page={nr}">В конец</a></li> '); ?>
                    </ul>
                </div>
            </div>
            <?php if (User::$auth): ?>
                <script src="/js/libs/ckeditor/ckeditor.js"></script>
                <button id="add-post" class="btn btn-primary">Ответить</button>
                <div class="row formpost" style="display: none;">
                    <div class="col-lg-12">
                        <form action="/forum/addPost" method="post" id="newpost">
                            <textarea name="text" id="ckedit"></textarea>
                            <input type="hidden" name="topic_id" value="<?= $this->data->topic->id ?>"/>
                            <input type="hidden" name="reply_to"/>
                            <button class="btn btn-primary">Ответить</button>
                            <button id="close" class="btn btn-danger">Закрыть</button>
                        </form>
                    </div>
                </div>
                <script>$(function () {
                        CKEDITOR.replace('ckedit');
                    })</script>
            <?php else: ?>
                <p>
                    <a class="fancybox" href="#login">Войдите</a> или
                    <a class="fancybox" href="#registration">зарегистрируйтесь</a>
                    для ответа в теме</p>
            <?php endif; ?>
        </div>
    </div>
</section>
<script>
    $(function () {

        $('#add-post').on('click', function () {
            $('.formpost').show();
            $(this).hide();
        });
        $('#close').on('click', function (e) {
            e.preventDefault();
            $('.formpost').hide();
            $('#add-post').show();
        });
        $('.post-delete').on('click', function () {
            if(confirm('Удалить сообщение?')){
                var id = $(this).data('id');
                $.ajax({
                    url: '/admin/forum/deletePost/' + id,
                    type: 'delete',
                    dataType: 'json',
                    success: function (data) {
                        if (data.success) {
                            $('#'+id).remove();
                        }
                    }
                })
            }

        })
    })
</script>