<?php

namespace Application\Controllers;

use Scandinaver\Classes\App;
use Scandinaver\Classes\Config;
use Scandinaver\Classes\Controller;
use Scandinaver\Classes\Options;
use Scandinaver\Classes\User;

/**
 * Class LogoutController
 * @package Application\Controllers
 *
 * Created by PhpStorm.
 * User: whiskey
 * Date: 06.02.15
 * Time: 0:52
 */
class LogoutController extends Controller {

    public function logout(){
        App::$session->clear();

        setcookie('token', 'w', time()-1000, '/', '.'.Options::$site);
        setcookie('u', '    w', time()-1000, '/', '.'.Options::$site);
        setcookie('user',  'w', time()-1000, '/', '.'.Options::$site);

        User::$auth = false;
        User::$role = 'user';
        header ('Location: http://'.HOST.'/');
    }
}