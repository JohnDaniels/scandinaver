<section id="main" class="container" style="margin-top: 50px;margin-bottom: 50px;">
        <?php //d($this->data->data, false); ?>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="avatar-wrapper-large center-block">
                    <div class="avatar"
                         style="background-image: url(<?= $this->data->data->photo ?>)">
                        <?php if(!$this->data->data->photo):?>
                            <p>Загрузить изображение профиля</p>
                        <?php endif;?>
                    </div>
                </div>
                <div id="uploadPhoto" style="display: none">
                    <form method="post" id="uploadPhotoForm" enctype="multipart/form-data">
                        <fieldset>
                            <div class="form-group">
                                <div class="col-md-10">
                                    <input id="inputFile" multiple="" type="file">
                                </div>
                            </div>
                            <div class="col-md-6 col-md-offset-3">
                                <label for="inputFile" class="col-md-6  btn btn-success no-margin">
                                  Выбрать</label>
                                <button class=" col-md-6 btn btn-success no-margin">Сохранить</button>
                            </div>
                        </fieldset>
                    </form>
                </div>
                <div class="col-md-12">
                    <div class="userinfo">
                        <table class="table table-striped table-bordered table-hover pages userlist">
                            <tr>
                                <td>Зарегистрирован:</td>
                                <td><?=$this->data->data->created_at?></td>
                            </tr>
                            <tr>
                                <td>Дней в сервисе:</td>
                                <td><?php echo round((date(time())-strtotime($this->data->data->created_at)) / 86400)?></td>
                            </tr>
                            <tr>
                                <td>Наборов открыто:</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Тем на форуме:</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Сообщений на форуме:</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Наборов создано:</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Средний результат:</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Рейтинг:</td>
                                <td>0</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="user_details">
                            <form method="post" action="/cabinet/update">
                                <div class="form-group row">
                                    <label for="lk-email" class="control-label">Email: (используется для входа на сайт)</label>
                                    <input class="form-control" id="lk-email" name="lk-email" type="text" value="<?=$this->data->data['email']?>"/>
                                </div>
                                <div class="form-group row">
                                    <label for="lk-login" class="control-label">Login: (можно указывать вместо email при входе)</label>
                                    <input class="form-control" id="lk-login" type="text" name="lk-login" value="<?=$this->data->data['login']?>"/>
                                </div>
                                <div class="form-group row">
                                    <label for="lk-name" class="control-label">Имя: (используется на форуме и почтовых рассылках)</label>
                                    <input class="form-control" id="lk-name" type="text" name="lk-name"value="<?=$this->data->data['name']?>"/>
                                </div>
                                <h4>Изменение пароля:</h4>
                                <div class="form-group row">
                                    <label for="lk-pass" class="control-label">Новый пароль:</label>
                                    <input class="form-control hidden" type="password"/>
                                    <input class="form-control col-md-6" id="lk-pass" name="pass" type="password" />
                                </div>
                                <div class="form-group row">
                                    <label for="lk-pass-repeat" class="control-label">Повторить пароль:</label>
                                    <input class="form-control col-md-6" id="lk-pass-repeat" name="pass-repeat" type="password" />
                                </div>
                                <button id="lk-update" class="col-md-6 btn btn-success no-margin">Сохранить</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>
<script>
    $(function(){
        $('.avatar-wrapper-large').on('click', function(){
            $('#uploadPhoto').toggle();
        });

        $('#inputFile').on('change', function(){
            if (this.files && this.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('.avatar p').hide();
                    $('.avatar').css('background-image', 'url('+e.target.result+')');
                };
                reader.readAsDataURL(this.files[0]);
            }
        });

        $('#uploadPhotoForm').on('submit', function(e){
            e.preventDefault();
            var formdata = new FormData();
            formdata.append('img', $('#inputFile')[0].files[0]);
            console.log(formdata);

            $.ajax({
                type: 'post',
                url: '/cabinet/uploadImage',
                data: formdata,
                processData: false,
                contentType: false,
                dataType: 'json',
                success: function(data){
                    toastr[(data.success == true)? 'success' : 'error'](data.msg);
                    toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-bottom-right",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "10",
                        "hideDuration": "1000",
                        "timeOut": "30000",
                        "extendedTimeOut": "300000",
                        "showEasing": "linear",
                        "hideEasing": "linear",
                        "showMethod": "slideDown",
                        "hideMethod": "fadeOut"
                    };
                }
            })
        });
        $('#lk-update').on('click', function(e){

            var pass1 = $('#lk-pass').val();
            var pass2 = $('#lk-pass-repeat').val();

            if((pass1 != '' || pass2 !='') && pass1 != pass2){
                toastr['error']('Пароли не совпадают!');
                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-bottom-right",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "10",
                    "hideDuration": "1000",
                    "timeOut": "3000",
                    "extendedTimeOut": "3000",
                    "showEasing": "linear",
                    "hideEasing": "linear",
                    "showMethod": "slideDown",
                    "hideMethod": "fadeOut"
                };
                $('#lk-pass').parents('.form-group').addClass('has-error');
                $('#lk-pass-repeat').parents('.form-group').addClass('has-error');
                return false;
            }
            else{
                return true;
            }
        })
    });
</script>