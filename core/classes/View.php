<?php

namespace Scandinaver\Classes;
use Symfony\Component\HttpFoundation\Response;
/**
 * Created by PhpStorm.
 * User: whiskey
 * Date: 23.11.14
 * Time: 19:29
 */
class View {

    /**
     * @var array
     */
    public  $template;

    /**
     * @var Data
     */
    public $data = '';

    /**
     * @var string
     */
    protected $layout = '';

    /**
     * имя вызывающего контроллера
     *
     * @ string
     */
    var $c_name;

    function __construct($c_name)
    {
        $this->c_name = $c_name;
        $this->data = new Data();
        //$this->template['index'] = TEMPLATE_PATH.$this->c_name.'/index.php';
    }

    /**
     * @param $layout
     * @return $this
     */
    function setLayout($layout)
    {
        $this->layout = TEMPLATE_PATH.'layouts/'.$layout.'.php';
        return $this;
    }

    /**
     * @param $name
     * @param $value
     * @return $this
     */
    function add($name, $value)
    {
        $this->data->setData($name, $value);
        return $this;
    }

    /**
     * @param $a_name
     * @return $this
     */
    function setTemplate($a_name)
    {
        $this->template = TEMPLATE_PATH.$this->c_name.'/'.$a_name.'.php';
        return $this;
    }

    function setHtmlTemplate($a_name)
    {
        $this->template = TEMPLATE_PATH.$this->c_name.'/'.$a_name.'.html';
        return $this;
    }

    function getTemplate()
    {
        include_once $this->template;
    }

    /**
     *
     */
    function render()
    {
        $file = ($this->layout) ?  include_once $this->layout : include_once $this->template;
        if(file_exists($file)){
            $response = new Response($file);
            $response->send();
        }
    }
}