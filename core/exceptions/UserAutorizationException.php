<?php
namespace Scandinaver\Exceptions;
use Exception;

/**
 * Created by PhpStorm.
 * User: user
 * Date: 23.03.2016
 * Time: 0:16
 */

class UserAutorizationException extends  Exception{

    protected $login;
    protected $pass;

    public function __construct($msg, $login = '', $pass = '', $code = 0)
    {
        parent::__construct($msg, $code);

        $this->login = $login;
        $this->pass = $pass;
    }

    public function getLogin()
    {
        return $this->login;
    }

    public function getPass()
    {
        return $this->pass;
    }
}