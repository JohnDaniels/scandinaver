<?php

namespace Scandinaver\Classes;
use Exception;

/**
 * Created by PhpStorm.
 * User: whiskey
 * Date: 29.11.14
 * Time: 5:51
 */

class Registry {

    /**
     * @var array
     */
    private static $vars = array();

    /**
     * @param $key
     * @param $value
     * @return bool
     * @throws Exception
     */
    public static function set($key, $value)
    {
        if (isset(self::$vars[$key]) == true)
            throw new Exception('Unable to set var `' . $key . '`. Already set.');

        self::$vars[$key] = $value;
        return true;
    }


    /**
     * @param $key
     * @return null
     */
    public static function get($key)
    {
        if (isset(self::$vars[$key]) == false)
            return null;

        return self::$vars[$key];
    }

    /**
     * @param $key
     * @return bool
     */
    public static function remove($key)
    {
        if(self::$vars[$key])
            unset(self::$vars[$key]);return true;
    }


    public static function monitor()
    {
        d(self::$vars);
    }
}