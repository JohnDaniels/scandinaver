<?php

namespace Scandinaver\Classes;

use Application\Models\Session;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Scandinaver\Events\GenericEvent;
use Scandinaver\Exceptions\DBExcteption;
use Scandinaver\Exceptions\UserAutorizationException;
use Scandinaver\Exceptions\UserRegistrationException;
use Symfony\Component\HttpFoundation\Request;
use DB;

/**
 * Created by PhpStorm.
 * User: whiskey
 * Date: 04.12.14
 * Time: 23:41
 */

class User
{
    public static $id = null;

    public static $pass = '';

    public static $login = '';

    public static $email = '';

    public static $_admin = false;

    public static $role = 'user';

    public static $auth = false;

    public static $avatar;

    public static $active = false;

    public static $active_to;

    /** @var Request  */
    protected $request = null;

    function __construct()
    {
        $this->request = Request::createFromGlobals();

        $this->checkAuthorize();
    }

    protected function checkAuthorize()
    {
        $token = $this->request->cookies->get('token');

        if(is_null($token)) return true;

        try{
            $session = Session::where(['token' => $token])->first();

            /**
             * @var \Application\Models\User $user
             */
            $user = $session->user;

            if(!$user) return true;

            self::$login  = $user->login;
            self::$email  = $user->email;
            self::$id     = $user->id;
            self::$auth   = true;
            self::$avatar = $user->avatar;
            self::$_admin = ($user->role == 'admin') ? true : false;
            self::$active_to = $user->active_to;
        }catch (ModelNotFoundException $e){
            return true;
        }

    }

    /**
     *Авторизация на сайте. ставим сессию и User
     * @param $login string
     * @param $pass string
     * @return \Application\Models\User
     * @throws UserAutorizationException
     * @internal param int|string $role integer
     */
    public static function autorize($login, $pass)
    {
        $checkLogin = DB::select('select count(id) as userExist from users where login = ? or email = ?', [$login, $login])[0];
        if(!$checkLogin->userExist)
            throw new UserAutorizationException('Пользователь не найден', $login, '', 1);

        /**
         * @var \Application\Models\User $user
         */
        $user = \Application\Models\User::whereRaw('(email = ? or login = ?) and pass = ?', [$login, $login, md5($pass)])
            ->first();

        if ($user) {
            l('autorized login: '.$login.' pass: '.$pass, 'success');
            App::$session->set('auth',  true);

            $token = md5($login.time());

            $session = new Session(['user_id' => $user->id, 'token' => $token]);
            $session->save();

            $crypt =  urlencode(base64_encode(openssl_encrypt(trim($user->toJson()), 'AES-256-CTR', Options::$cryptKey, true, Options::$cryptIv)));

            setcookie('token', $token, time()+60*60*24*30, '/', '.'.Options::$site);
            setcookie('user', $crypt, time()+60*60*24*30, '/', '.'.Options::$site);

            self::$login = $user->login;
            self::$auth = true;
            self::$_admin = ($user->role == 'admin') ? true : false;

         //   Requester::loginForumUser(['username' => $login, 'password' => $pass]);

            return  $user;
        } else {
            throw new UserAutorizationException('Неверный пароль', $login, $pass);
        }
    }

    /**
     * или вообще все тут переделать
     * слишком длинная цепочка
     * При создании юзера сразу задаем ему basic assets!
     * @param $params
     * @return bool
     * @throws Exception
     * @throws UserRegistrationException
     */
    public static function registration($params)
    {
        if($params['pass'] != $params['pass2'])
            throw new UserRegistrationException('Пароли не совпадают');

        self::$pass = $params['pass'];

        $params = array(
            'login'    => $params['login'],
            'email'    => $params['email'],
            'pass'     => md5($params['pass']),
            'openpass' => $params['pass'],
            'role'     => $params['role']
        );

        $checkLogin = DB::select('select count(id) as userExist from users where login = ?', [$params['login']]);
        $checkEmail = DB::select('select count(id) as userExist from users where email = ?', [$params['email']]);

        if($checkLogin[0]->userExist)
            throw new UserRegistrationException('Пользователь с таким логином уже зареган', $params['login'], '', 1);

        if($checkEmail[0]->userExist)
            throw new UserRegistrationException('Пользователь с таким email уже зареган', '', $params['email'], 2);

        $user = new \Application\Models\User($params);

        if ($user->save()) {
            Requester::sendRegInfo($user->id);
            Requester::createForumUser($params);
            App::$dispatcher->dispatch('registration.event', new GenericEvent(null, $params));
            self::autorize($params['email'], $params['openpass']);
        } else {
            throw new DBExcteption('Ошибка добавления юзера в бд', null , null);
        }
    }


    public static function updateCookies()
    {
        $user = \Application\Models\User::find(self::$id);
        $crypt =  urlencode(base64_encode(openssl_encrypt(trim($user->toJson()), 'AES-256-CTR', Options::$cryptKey, true, Options::$cryptIv)));
        setcookie('user', $crypt, time()+60*60*24*30, '/', '.'.Options::$site);
    }
}