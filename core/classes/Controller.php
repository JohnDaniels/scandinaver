<?php
/**
 * Created by PhpStorm.
 * User: whiskey
 * Date: 23.11.14
 * Time: 14:46
 */
namespace Scandinaver\Classes;

use Application\Models\Meta;
use ReflectionClass;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

class Controller {
    /**
     * @var View
     */
    public $view = null;

    protected $c_name = '';

    protected $answer = [];

    /**
     * @var Request
     */
    protected $request;

    protected function init()
    {
        $this->request = Request::createFromGlobals();
    }

    /**
     * @param string $a_name
     * @param array $args
     */
    function __construct($a_name = 'index', $args = array())
    {
        $this->init();
        $reflect = new ReflectionClass($this);

        $this->c_name = strtolower(substr($reflect->getShortName(), 0, -10));

        if(App::$moduleName == 'public' && !User::$auth && ($this->c_name == 'remind'))
            $this->setView($this->c_name);

        //TODO: пиздец. решить это все через router
        elseif(App::$moduleName == 'public' && !User::$auth && ($this->c_name == 'api'))
            $this->setView($this->c_name);


       // elseif(App::$moduleName == 'public' && !User::$auth && ($this->c_name != 'index'))
       //     App::goHome();


        if(App::$moduleName == 'admin' && !User::$_admin && ($this->c_name != 'vue'))
            App::goHome('admin');


        $this->setView($this->c_name); //TODO: убрать и вызывать вручную
        $this->setSeo();
    }

    public function setSeo()
    {
        $url = $this->request->getRequestUri();

        $data = Meta::where('url', $url)->get();

        if(count($data) > 0){
            $this->view->add('title',       $data[0]['title']);
            $this->view->add('description', $data[0]['description']);
            $this->view->add('keywords',    $data[0]['keywords']);
        }
    }

    /**
     * @param $c_name
     * @return View
     */
    function setView($c_name)
    {
        $this->view = new View($c_name);
        return $this->view;
    }

    function index()
    {

    }

    function getAction($action)
    {
        return $this->$action;
    }

    /**
     * Отправить json-ответ
     * @param array $message
     */
    public function send($message = null)
    {
        $response = new JsonResponse(($message) ? $message : $this->answer);
        $response->headers->set('Content-Type', 'application/json');
        $response->send();
    }

    public function pageNotFound()
    {
        $response = new Response();
        $response->setStatusCode(404);
        $response->headers->set('Content-Type', 'text/html');
        $response->send();
        require_once BASE_URL.'/404.php';
    }

    public function redirect($page)
    {
        header('Location: https://'.HOST.'/'.$page);
    }

    public function sredirect($page)
    {
        $response = new RedirectResponse($page);
        $response->send();
    }
} 