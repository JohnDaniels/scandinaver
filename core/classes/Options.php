<?php
namespace Scandinaver\Classes;

use Application\Models\Config;

class Options
{
    static $debug = [];

    public static $dbhost = 'localhost';
    public static $dbuser = 'root';
    public static $dbpass = '9922388';
    public static $dbase  = 'scandinaver';

    public static $config;

    public static $_main_site = 'https://scandinaver.org';
    public static $_forum     = 'https://forum.scandinaver.org';
    public static $icelandic  = 'https://icelandic.scandinaver.org';
    public static $sweden     = 'https://sweden.scandinaver.org';
    public static $norwegian  = 'https://norway.scandinaver.org';
    public static $danish     = 'https://danish.scandinaver.org';

    static $site;
    static $domain;
    static $routes;

    public static $cryptKey = 'vPnMEnkUUnKAPNjW';
    public static $cryptIv  = 'vPnMEnkUUnKAPNjW';

    static function init()
    {
        switch ($_SERVER['HTTP_HOST']){
            case 'scandinaver.local':
                self::$dbpass = '';
                self::$dbuser = 'root';
                self::$dbhost = 'localhost';
                self::$dbase  = 'scandinaver';
                self::$_main_site = 'https://scandinaver.local';
                self::$_forum     = 'https://forum.scandinaver.local';
                self::$icelandic  = 'https://icelandic.scandinaver.local';
                self::$sweden     = 'https://sweden.scandinaver.local';
                self::$norwegian  = 'https://norwegian.scandinaver.local';
                self::$danish     = 'https://danish.scandinaver.local';
                self::$site   = 'scandinaver.local';
                self::$domain = 'scandinaver.local';
                break;
            case 'scandinaver.org':
                self::$site = 'scandinaver.org';
                self::$domain = 'scandinaver.org';
                break;
        }

        self::loadRoutes();
    }

    public static function loadConfig()
    {
        self::$config = new \stdClass();

        foreach ( Config::all()->toArray() as $item)
            self::$config->{$item['name']} = $item['value'];
    }

    /**
     *
     */
    private static function loadRoutes()
    {
        self::$routes = require_once  __DIR__. '/../configuration/routes.php';
    }
}