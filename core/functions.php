<?php

use Application\Models\Log;

function d($var, $stop = true){
    echo '<pre>';
    print_r($var);
    echo '</pre>';
    if($stop) die;
}

/**
 * @param $message
 * @param string $type info|warning|danger
 */
function l($message, $type = '')
{
    $date = '['.date('d.m.d H:i:s Y', time()).']...';

    if(is_writable( __DIR__. '/../log.txt'))
    {
        $f = fopen(__DIR__. '/../log.txt', 'a+');
        $log = $date . $message . "\n";
        fwrite($f, $log);
        fclose($f);
    }

    $log = new Log(['message' => $message, 'type' => $type]);
    $log->save();
}
