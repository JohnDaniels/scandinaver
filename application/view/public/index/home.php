<section>
<div class=" parallax-window"  data-parallax="scroll" data-image-src="/img/head-bg.jpg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="intro-message" style="color: rgb(255, 255, 255);">
                    <h1>Scandinaver</h1>
                    <p>
                        "Я изучаю исландский язык не для того, чтобы научиться политике, приобрести военные знания и т. п.,<br/>
                        но для того, чтобы научиться образу мыслей мужа, для того, чтобы избавиться<br/>
                        от укоренившегося во мне с детства благодаря воспитанию духа убожества и рабства,<br/>
                        для того, чтобы закалить мысль и душу так, чтобы я мог без трепета идти навстречу опасности<br/>
                        и чтобы моя душа предпочла скорее расстаться с телом,<br/>
                        чем отречься от того, в истинности и правоте чего она непоколебимо убеждена".<br/>
                        Rasmus Christian Rask
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content-section-a">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-sm-6">
                <hr class="section-heading-spacer">
                <div class="clearfix"></div>
                <h2 class="section-heading">Lorem ipsum dolor sit amet</h2>
                <p class="lead">Lorem ipsum dolor sit amet,
                    consectetur adipiscing elit. Donec id lectus sit amet felis ultrices eleifend.
                    Suspendisse accumsan mauris a sem ultricies faucibus. Cras pharetra metus nec tempor interdum.
                    Proin tristique porta ultricies. Cras et sollicitudin nulla.
                    Pellentesque consequat nisl nec felis rutrum, sit amet dictum ligula iaculis.
                    Praesent ut dolor pulvinar mauris sodales egestas in eu ligula. </p>
            </div>
            <div class="col-lg-5 col-lg-offset-2 col-sm-6">
                <img class="img-responsive illustration" src="/img/dear-1.png" alt="">
            </div>
        </div>
    </div>
</div>
<div class="content-section-b">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-lg-offset-1 col-sm-push-6  col-sm-6">
                <hr class="section-heading-spacer">
                <div class="clearfix"></div>
                <h2 class="section-heading">Lorem ipsum dolor sit amet</h2>
                <p class="lead">Sed vel mi ac felis aliquam lobortis.
                    Vivamus iaculis mauris faucibus tortor lobortis tempor.
                    Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;
                    Aenean sit amet placerat neque, in faucibus turpis. Sed sagittis eleifend lacinia.
                    Nam ut commodo velit, sit amet rutrum nulla. Fusce malesuada enim arcu, et vehicula nibh commodo vel.
                    Vestibulum tristique eros risus, eu laoreet lectus fringilla ac.</p>
            </div>
            <div class="col-lg-5 col-sm-pull-6  col-sm-6">
                <img class="img-responsive illustration" src="/img/dear-2.png" alt="">
            </div>
        </div>
    </div>
</div>
<div class="content-section-a">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-sm-6">
                <hr class="section-heading-spacer">
                <div class="clearfix"></div>
                <h2 class="section-heading">Lorem ipsum dolor sit amet</h2>
                <p class="lead">Sed vel mi ac felis aliquam lobortis.
                    Vivamus iaculis mauris faucibus tortor lobortis tempor.
                    Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;
                    Aenean sit amet placerat neque, in faucibus turpis. Sed sagittis eleifend lacinia.
                    Nam ut commodo velit, sit amet rutrum nulla. Fusce malesuada enim arcu, et vehicula nibh commodo vel.
                    Vestibulum tristique eros risus, eu laoreet lectus fringilla ac.</p>
            </div>
            <div class="col-lg-5 col-lg-offset-2 col-sm-6">
                <img class="img-responsive illustration" src="/img/dear-3.png" alt="">
            </div>
        </div>
    </div>
</div>
<div class="content-section-b">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-lg-offset-1 col-sm-push-6  col-sm-6">
                <hr class="section-heading-spacer">
                <div class="clearfix"></div>
                <h2 class="section-heading">Lorem ipsum dolor sit amet</h2>
                <p class="lead">Sed vel mi ac felis aliquam lobortis.
                    Vivamus iaculis mauris faucibus tortor lobortis tempor.
                    Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;
                    Aenean sit amet placerat neque, in faucibus turpis. Sed sagittis eleifend lacinia.
                    Nam ut commodo velit, sit amet rutrum nulla. Fusce malesuada enim arcu, et vehicula nibh commodo vel.
                    Vestibulum tristique eros risus, eu laoreet lectus fringilla ac.</p>
            </div>
            <div class="col-lg-5 col-sm-pull-6  col-sm-6">
                <img class="img-responsive illustration" src="/img/dear-4.png" alt="">
            </div>
        </div>
    </div>
</div>
<div class="content-section-b" id="langauges">
    <div class="container full-width">
        <div class="row">
            <div class="col-lg-3 col-sm-6 icelandic text-center">
                <div class="card">
                    <div class="card-header">
                        <a href="https://icelandic.scandinaver.org">
                            <p style="font-family: 'Julius Sans One',sans-serif;">icelandic.scandinaver.org</p>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                    <a href="https://icelandic.scandinaver.org">
                        <div class="illustrate"></div>
                    </a>
                    <p class="lead">Здесь нет ни насекомых, ни деревьев, вообще ничего.
                        Люди живут здесь, только потому что здесь родились.
                        Понимай, как хочешь, то ли божья кара, то ли благодать.</p>
                    <div class="clearfix"></div>
                    <div class="card-footer text-right">

                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 sweden text-center">
                <div class="card">
                    <div class="card-header">
                        <a href="https://swedish.scandinaver.org">
                            <p style="font-family: 'Julius Sans One',sans-serif;">swedish</p></a>
                    </div>
                    <a href="https://swedish.scandinaver.org">
                        <div class="illustrate"></div>
                    </a>
                    <p class="lead">— У меня есть адрес, — сказал он и протянул ему бумажку. [...] — Сейчас они живут в
                        Швеции.
                        Этого только не хватало, подумал Карл.
                        Швеция, страна с самыми крупными на свете комарами и с самой гадкой на свете едой.</p>
                    <div class="clearfix"></div>
                    <div class="card-footer text-right">

                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 norwegian disabled">
                <div class="card">
                    <div class="illustrate"></div>
                    <p class="lead">В разработке</p>
                    <div class="card-footer text-right">

                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 finnish disabled">
                <div class="card">
                    <div class="illustrate"></div>
                    <p class="lead">В разработке</p>
                <div class="card-footer text-right">

                </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if(!Scandinaver\Classes\User::$auth):?>
    <div class="banner">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">

                </div>
                <div class="col-lg-12 text-center">
                    <a href="#registration"  class="btn btn-default btn-lg fancybox">
                        <span class="network-name">Регистрация</span></a>
                    <a href="#login"  class="btn btn-default btn-lg fancybox">
                        <span class="network-name">Вход</span></a>
                </div>
            </div>
        </div>
    </div>
<?php endif;?>
</section>
<script>
    $(function(){
        $('.content-section-b .img-responsive').boxLoader({
            direction:"x",
            position: "-5%",
            effect: "fadeIn",
            duration: "1s",
            windowarea: "100%"
        });
        $('.content-section-a .img-responsive').boxLoader({
            direction:"x",
            position: "5%",
            effect: "fadeIn",
            duration: "1s",
            windowarea: "100%"
        });
        $('.icelandic').boxLoader({
            direction:"y",
            position: "5%",
            effect: "fadeIn",
            duration: "0.3s",
            windowarea: "70%"
        });
        $('.sweden').boxLoader({
            direction:"y",
            position: "10%",
            effect: "fadeIn",
            duration: "0.5s",
            windowarea: "70%"
        });
        $('.norwegian').boxLoader({
            direction:"y",
            position: "15%",
            effect: "fadeIn",
            duration: "0.7s",
            windowarea: "70%"
        });
        $('.finnish').boxLoader({
            direction:"y",
            position: "20%",
            effect: "fadeIn",
            duration: "1s",
            windowarea: "70%"
        });
        $(window).scroll(function(){
            var offset = 255 - $(window).scrollTop();
            $('.intro-message').css('color', 'rgb('+offset+', '+offset+', '+offset+')');
        });
    })
</script>