<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 28.05.2015
 * Time: 21:39
 */
namespace Scandinaver\Classes;
use PHPMailer\PHPMailer;

class Mailer extends PHPMailer\PHPMailer{

    private $template = '';
    private $user_name = '';
    private $user_email = '';
    private $user_pass = '';

    public  function sendRegistrationMail($params){

        $this->user_name = $params['username'];
        $this->user_email = $params['email'];
        $this->user_pass = $params['pass'];

        $this->template = '<html xmlns="http://www.w3.org/1999/xhtml">
                                <head>
                                    <meta http-equiv="Content-Type" content="text/html" charset="utf-8">
                                    <meta name="viewport" content="width=device-width, initial-scale=1">
                                    <title>Scandinaver</title>
                                    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Julius+Sans+One">
                                </head>
                                <body>
                                    <table border="0" cellspacing="0" cellpadding="0" style="width: 100%;height: 100%;">
                                        <tbody>
                                            <tr>
                                                 <td style="font-family: Helvetica,Times,serif;width:100%">
                                                    <table style="border:1px solid #c62828; margin: 0px auto;" cellpadding="0">
                                                         <tbody>
                                                            <tr>
                                                                <td colspan="5"  style="height: 80px;">
                                                                     <table border="0" cellspacing="0" cellpadding="0"  style="height: 100%;width: 100%;">
                                                                         <tbody>
                                                                            <tr style="background-color: #c62828">
                                                                            <td  style="color: rgb(255, 255, 255);
																					height: 62px;
																					width: 80%;
																					text-align: left;
																					font-family: Julius Sans One,sans-serif;
																					font-size: 2em;
																					padding-left: 20px;">
                                                                            	SCANDINAVER
                                                                            </td>
                                                                                <td style="height:62px; width:20%">
                                                                                    <img src="http://www.scandinaver.org/application/assets/img/dear-3-white.png" height="50" width="30">
                                                                                </td>
                                                                            </tr>
                                                                         </tbody>
                                                                     </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="5" style="font-size: 24px;
																			height: 20px;
																			padding: 10px;
																			text-align: center;">
                                                                	<p style="color:#666;">
                                                                        Halló, '.$this->user_name.'
                                                                    </p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="5" style="padding:25px; font-size: 20px;height: 40px;">
                                                                	<p style="color:#666;">Вы зарегистрировались на сайте scandinaver.org</p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="5" style="padding: 0 25px; font-size: 16px;">
                                                                	<p style="color:#666;">Используйте эти данные для входа:</p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="5" style="padding: 0 25px; font-size: 16px;height: 41px;">
                                                                    <p style="color:#666;margin: 0px;">login: '.$this->user_email.'</p>
                                                                    <p style="color:#666;margin: 0 0 10px 0;">password: '.$this->user_pass.'</p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="5" style="padding:25px; font-size: 20px;height: 40px;">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="5" style="padding:25px; font-size: 20px;height: 40px;">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="5" style="padding:25px; font-size: 20px;height: 40px;">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 20%;text-align: center;">
                                                                	<img src="http://www.scandinaver.org/application/assets/img/dear-5.png" height="50" width="30">
                                                                </td>
                                                                <td style="width: 20%;text-align: center;">
                                                                	<img src="http://www.scandinaver.org/application/assets/img/dear-2.png" height="50" width="30">
                                                                </td>
                                                                <td style="width: 20%;text-align: center;">
                                                                	<img src="http://www.scandinaver.org/application/assets/img/dear-3.png" height="50" width="30">
                                                                </td>
                                                                <td style="width: 20%;text-align: center;">
                                                                	<img src="http://www.scandinaver.org/application/assets/img/dear-4.png" height="50" width="30">
                                                                </td>
                                                                <td style="width: 20%;text-align: center;">
                                                                	<img src="http://www.scandinaver.org/application/assets/img/dear-1.png" height="50" width="30">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="5"  style="height: 50px;">
                                                                    <table style="; background-color: #c62828;font-size: 13px;width: 100%;height: 100%;">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td style="width: 50%;padding: 10px;">
                                                                                     <a style="color:#fff;" target="_blank" href="https://instagram.com/scandinaver">Instagram</a>
                                                                                     <a style="color:#fff;" target="_blank" href="https://vk.com/scandinaver">VK</a>
                                                                                     <a style="color:#fff;" target="_blank" href="https://twitter.com/scandinaver">Twitter</a>
                                                                                     <a style="color:#fff;" target="_blank" href="https://www.facebook.com/pages/Icelandreams/1603827529904227">Facebook</a>
                                                                                </td>
                                                                                <td style="width: 50%;padding: 10px;color:#fff;text-align: right;">support@scandinaver.org</td>

                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                         </tbody>
                                                    </table>
                                                 </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </body>
                           </html>';

        $this->isHTML(true);
        $this->CharSet="utf-8";
        $this->From = 'support@scandinaver.org';
        $this->FromName = 'scandinaver.org';
        $this->Subject = 'Регистрация на сайте scandinaver.org';
        $this->Body = $this->template;
        $this->clearAllRecipients();
        $this->addAddress($this->user_email);
        $this->send();
    }

    public function sendRegistrationMailToAdmin($params)
    {
        $this->user_name = $params['username'];
        $this->user_email = $params['email'];
        $this->template = 'Новый пользователь - login: '.$this->user_name.', email: '.$this->user_email;
        $this->CharSet="utf-8";
        $this->From = 'support@scandinaver.org';
        $this->FromName = 'Администрация';
        $this->Subject = 'Регистрация на сайте scandinaver.org';
        $this->Body = $this->template;
        $this->clearAllRecipients();
        $this->addAddress('support@scandinaver.org');
        $this->send();
    }

    public function sendRestoreMail($params)
    {
        $this->user_name = $params['username'];
        $this->user_email = $params['email'];
        $link = $params['link'];

        $this->template = 'Здравствуйте, '.$this->user_name.', вы запросили восстановление пароля на сайте icelandreams.ru.<br></br>
                            Для смены пароля перейдите по ссылке <a target="_blank" href="'.$link.'">'.$link.'</a>.<br></br>
                             Если вы не запрашивали восстановление пароля и не понимаете о чем речь, просто проигнорируйте это письмо.<br></br>
                             support@icelandreams.ru.';

        $this->isHTML(true);
        $this->CharSet="utf-8";
        $this->From = 'support@icelandreams.ru';
        $this->FromName = 'icelandreams.ru';
        $this->Subject = 'Восстановление пароля на сайте icelandreams.ru';
        $this->Body = $this->template;
        $this->clearAllRecipients();
        $this->addAddress($this->user_email);
        $this->send();
    }

    public function sendRestoreMailToAdmin($params)
    {
        $this->user_name = $params['username'];
        $this->user_email = $params['email'];
        $this->template = 'пользователь ' . $this->user_name . ', email: ' . $this->user_email . ', запросил восстановление пароля';
        $this->CharSet = "utf-8";
        $this->From = 'support@icelandreams.ru';
        $this->FromName = 'Администрация';
        $this->Subject = 'Восстановление пароля на сайте icelandreams.ru';
        $this->Body = $this->template;
        $this->clearAllRecipients();
        $this->addAddress('support@scandinaver.org');
        $this->send();
    }

    public function sendMessageNotify($params)
    {
        $this->CharSet = "utf-8";
        $this->isHTML(true);
        $this->From = 'support@scandinaver.org';
        $this->FromName = 'Администрация';
        $this->Subject = 'Новое сообщение';
        $this->Body = 'Имя:'.$params['name'].'<br>
                        Сообщение: <br>'.$params['message'];
        $this->clearAllRecipients();
        $this->addAddress('support@scandinaver.org');
        $this->send();
    }
}