<?php
use Scandinaver\Classes\User;
?>
<section id="main" class="container" style="margin-top: 50px;margin-bottom: 50px;">
    <header class="row forum-head">
        <div class="col-md-12">
            <div class="row">
                <h3 class="no-margin pull-left"><?= $this->data->forum['name'] ?></h3>
                <div class="pull-right">
                    <?php if (User::$auth): ?>
                        <button id="addTopic" class="btn btn-defult pull-right no-margin">Добавить тему</button>
                    <?php else: ?>
                        <p class="text-muted small">Войдите или зарегистрируйтесь, чтобы добавлять темы</p>
                    <?php endif ?>
                </div>
            </div>
            <div class="row">
                <div class="breadcrumbs pull-left col-lg-5 col-md-4 col-sm-6 hidden-xs">
                    <ul class="breadcrumb">
                        <li>
                            <a href="/forum">Форум</a>
                        </li>
                        <li>
                            <?= $this->data->forum['name'] ?>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12 pull-right text-right">
                    <ul class="pagination pull-right">
                        <?php echo $this->data->pager->first(' <li class="page-start"><a href="?page={nr}">В начало</a></li> '); ?>
                        <?php echo $this->data->pager->numbers(' <li><a href="?page={nr}">{nr}</a></li>', ' <li class="active"><a href="javascript:void(0)">{nr}</a></li>'); ?>
                        <?php echo $this->data->pager->last(' <li class="page-end"><a href="?page={nr}">В конец</a></li> '); ?>
                    </ul>
                </div>
            </div>
        </div>
    </header>
    <div style="display: none">
        <form id="addTopicForm" action="/forum/addTopic" method="post">
            <div class="form-group">
                <input class="form-control" type="text" name="topicName" placeholder="Название темы"/>
            </div>
            <div class="form-group">
                <textarea name="topicText" id="topicText" cols="30" rows="10"></textarea>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-success" value="Добавить"/>
                <button id="closeForm" class="btn btn-warning">Закрыть</button>
            </div>
            <input type="hidden" name="uid" value="<?= User::$id ?>"/>
            <input type="hidden" name="forumId" value="<?= $this->data->forum['id'] ?>"/>
        </form>
    </div>
    <div class="row forum-section">
        <header class="col-md-12">
            <div class="row forum-header">
                <div class="col-lg-9 col-md-8 col-sm-8 hidden-xs">Тема</div>
                <div class="col-lg-3 col-md-4 col-sm-4 hidden-xs vcenter">Последнее сообщение</div>
            </div>
        </header>
        <div class="col-md-12">
            <div class="forums-wrapper row">
                <?php foreach ($this->data->topics as $topic): ?>
                    <?php if (!$topic->active && !User::$_admin) continue; ?>
                    <div class="row forum" id="<?= $topic->id ?>">
                        <div class="col-lg-9 col-md-8 col-sm-8 col-xs-12 forum-info">

                            <div class="row forum-name">
                                <div class="col-md-12">
                                    <i class="ion-ios-chatboxes-outline hidden-xs"></i>

                                    <div class="vcenter topic-name">
                                        <a href="/forum/<?= $this->data->forum->id ?>/<?= $topic->id ?>">
                                            <?= $topic->name ?>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-6 col-md-4 col-sm-4 col-xs-5">
                                    <div class="col-md-2 col-sm-2 col-xs-4 no-padding">
                                        <div class="avatar-wrapper-small no-margin">
                                            <div class="avatar"
                                                 style="background-image: url(<?= $topic->author->avatar ?>)"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-10 col-xs-8">
                                        <p class="author-info">
                                            <?= $topic->author->name ?>
                                        </p>
                                    </div>
                                </div>

                                <div class=" col-lg-6 col-md-8 col-sm-8 col-xs-7 forum-properties">
                                    <div class="forum-stats small no-margin">
                                        <div>
                                            <?= $topic->created_at->format('Y.m.d H:m') ?>
                                        </div>
                                        <div>
                                            Просмотров: <span><?= $topic->view ?></span>
                                        </div>
                                        <div>
                                            Ответов: <span><?= count($topic->posts) ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-4 hidden-xs">
                            <div class="row">
                                <div class="last-topic-link vcenter">
                                    <i class="ion-ios-redo-outline"></i>
                                    <a href="/forum/<?=$topic->forum->id?>/<?=$topic->id?>?page=<?= ceil($topic->posts->count() / \Scandinaver\Classes\Options::$config->forum_posts_on_page)?>">
                                        <?= $topic->posts[0]->created_at ?>
                                    </a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <p class="author-info">
                                        <?= $topic->posts[0]->author->name ?>
                                    </p>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <div class="avatar-wrapper-small no-margin">
                                        <div class="avatar"
                                             style="background-image: url(<?= $topic->last_avatar ?>)"></div>
                                    </div>
                                </div>
                            </div>
                            <?php if (User::$_admin): ?>
                                <div class="forum-control">
                                    <span data-name="active" style="font-size: 26px;" data-id="<?= $topic->id ?>"
                                          class="ion-ios-pulse topic-active-edit pointer
                                              <?php if ($topic->active): ?>
                                                    text-success
                                              <?php else: ?>
                                                    text-danger
                                              <?php endif; ?>">

                                    </span>
                                    <i class="ion-ios-close-empty pointer topic-delete" data-id="<?= $topic->id ?>"></i>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 text-right">
            <ul class="pagination">
                <?php echo $this->data->pager->first(' <li class="page-start"><a href="?page={nr}">В начало</a></li> '); ?>
                <?php echo $this->data->pager->numbers(' <li><a href="?page={nr}">{nr}</a></li>', ' <li class="active"><a href="javascript:void(0)">{nr}</a></li>'); ?>
                <?php echo $this->data->pager->last(' <li class="page-end"><a href="?page={nr}">В конец</a></li> '); ?>
            </ul>
        </div>
    </div>
</section>
<script src="/js/libs/ckeditor/ckeditor.js"></script>
<script>
    $(function () {
        CKEDITOR.replace('topicText');
        var body = $('body');

        $('#addTopic').on('click', function () {
            $('#addTopicForm').parent().show();
        });

        $('#closeForm').on('click', function (e) {
            e.preventDefault();
            $('#addTopicForm').parent().hide();
            $('[name="topicName"],[name="topicText"]').val('');
        });

        body
            .on('click', '.topic-active-edit', function () {
                var pulse = $(this);
                var name = 'active';
                var id = $(this).data('id');
                var value = (pulse.hasClass('text-success') ? 0 : 1);

                $.ajax({
                    url: '/admin/forum/ajaxEdittopic',
                    type: 'post',
                    data: ({'id': id, 'name': name, 'value': value}),
                    dataType: 'json',
                    success: function (data) {
                        if (data.success) {
                            if (value === 0)
                                pulse.removeClass('text-success').addClass('text-danger');
                            else
                                pulse.removeClass('text-danger').addClass('text-success');
                        }
                    }
                });
            })
            .on('click', '.topic-delete', function(){
                if(confirm('Удалить топик?')){
                    var id = $(this).data('id');
                    $.ajax({
                        url: '/admin/forum/deleteTopic/' + id,
                        type: 'delete',
                        dataType: 'json',
                        success: function (data) {
                            if (data.success) {
                                $('#'+id).remove();
                            }
                        }
                    })
                }
            })
    })
</script>