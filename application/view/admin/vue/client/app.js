import Vue from 'vue'
import NProgress from 'vue-nprogress'
import {sync} from 'vuex-router-sync'
import App from './App.vue'
import router from './router'
import store from './store'
import * as filters from './filters'
import {TOGGLE_SIDEBAR} from 'vuex-store/mutation-types'
import VueResource from 'vue-resource'
import VueProgressBar from 'vue-progressbar'

import VueQuillEditor from 'vue-quill-editor'

// require styles
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'

Vue.router = router

const options = {
  color: '#20A0FF',
  failedColor: '#874b4b',
  thickness: '5px',
  transition: {
    speed: '0.2s',
    opacity: '0.6s'
  },
  location: 'top',
}

Vue.use(VueQuillEditor, /* { default global options } */)
Vue.use(VueProgressBar, options)

Vue.use(NProgress)

Vue.use(VueResource)

// Enable devtools
Vue.config.devtools = true

Vue.http.options.emulateJSON = true

sync(store, router)

const nprogress = new NProgress({parent: '.nprogress-container'})

const {state} = store

router.beforeEach((route, redirect, next) => {
    if (state.app.device.isMobile && state.app.sidebar.opened) {
        store.commit(TOGGLE_SIDEBAR, false)
    }
    next()
})

Object.keys(filters).forEach(key => {
    Vue.filter(key, filters[key])
})

const app = new Vue({
    router,
    store,
    nprogress,
    ...App
})

export {app, router, store}
