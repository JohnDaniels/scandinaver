<?php

namespace Application\Controllers;

use Scandinaver\Classes\App;
use Scandinaver\Classes\Controller;

/**
 * Class LoginController
 * @package Application\Controllers
 *
 * Created by PhpStorm.
 * User: whiskey
 * Date: 31.01.15
 * Time: 3:42
 */
class LoginController extends Controller{

    public function index()
    {
        $this->view->setLayout('main')
                    ->setTemplate('login')
                    ->render();
    }

    public function login()
    {
        if($_POST['login'])
        {
            $user = App::$user;
            $login = $_POST['login'];
            $pass = md5($_POST['pass']);

            $r = $user->autorize($login, $pass);

            if($r){
               header('Location: http://'.HOST.'localhost/cards/assets');
            }
        }
    }
}