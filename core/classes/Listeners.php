<?php

namespace Scandinaver\Classes;

use Scandinaver\Events\GenericEvent;
use Symfony\Component\EventDispatcher\Event;

class Listeners {

    public function onTestAction(Event $event)
    {
        d($event);
    }

    public function onRegistrationAction(GenericEvent $event){

        $sender = new Mailer();
        $sender->sendRegistrationMail(array('username' => $event->getArgument('login'),
                                            'email' => $event->getArgument('email'),
                                            'pass' => $event->getArgument('openpass')));

        $sender->sendRegistrationMailToAdmin(array('username' => $event->getArgument('login'),
                                                    'email' =>  $event->getArgument('email')));

        l('new user registration. login - ' . $event->getArgument('login') . '; email - ' .$event->getArgument('email'));
    }

    public function onNewMessageAction(GenericEvent $event)
    {
        $sender = new Mailer();
        $sender->sendMessageNotify(['name' => $event->getArgument('name'),
                                    'message' => $event->getArgument('message')]);

        l('new message from '.$event->getArgument('name'));
    }

    public function onUserDelete(GenericEvent $event)
    {
        $event->getSubject()->comments()->delete();
        $event->getSubject()->sessions()->delete();

        Requester::sendRemoveUser($event->getSubject()->id);

        l(' user id '.$event->getSubject()->id.' removed');
    }
}