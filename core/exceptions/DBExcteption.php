<?php
namespace Scandinaver\Exceptions;
use Exception;

/**
 * Created by PhpStorm.
 * User: user
 * Date: 23.03.2016
 * Time: 0:25
 */

class DBExcteption extends Exception{

    private $sql;

    public function __construct($msg, $sql, $code)
    {
        parent::__construct($msg, $code);

        $this->sql = $sql;
    }

    public function getSql()
    {
        return $this->sql;
    }
}