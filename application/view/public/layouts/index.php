<?php
use Scandinaver\Classes\Registry;
use Scandinaver\Classes\User;
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?= $this->data->description?>">
    <meta name="keywords" content="<?=$this->data->keywords?>">
    <title><?=$this->data->title?></title>
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Julius+Sans+One">
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans&subset=latin,cyrillic">
    <link href="/css/style.min.css" rel="stylesheet">
    <script src="/js/script.js"></script>
    <?php if (User::$_admin) echo(Registry::get('renderer')->renderHead()) ?>
</head>
<body>
    <nav id="sidr-left" style="left: -260px;position: fixed;">
        <ul>
            <li>
                <a href="/">Главная</a>
            </li>
            <li>
                <a href="/#langauges">Языки</a>
            </li>
            <li>
                <a href="<?= \Scandinaver\Classes\Options::$_forum?>">Форум</a>
            </li>
          <!--  <li>
                <a href="/downloads">Материалы</a>
            </li> -->
            <li>
                <a href="/blog">Статьи</a>
            </li>
            <?php if (!User::$auth): ?>
                <li><a href="#login" class="fancybox">Вход</a></li>
                <li><a href="#registration" class="fancybox">Регистрация</a></li>
            <?php else: ?>
                <li><a href="/logout">Выход</a></li>
                <li>
                    <a href="/cabinet">
                        <?= User::$login ?>
                        <div class="avatar-wrapper-small"><div class="avatar" style="background-image: url(<?= User::$avatar; ?>)"></div></div>
                    </a>
                </li>
            <?php endif; ?>
            <?php if (User::$_admin): ?>
                <li><a href="/admin">adminpanel</a></li>
            <?php endif; ?>
        </ul>
    </nav>
<div id="panel">
    <nav class="navbar navbar-default" role="navigation">
        <div class="logo-wrapper">
            <a href="/">
                <div class="logo"></div>
            </a>
        </div>
        <div class="container">
            <div class="navbar-header pull-left">
                <a id="left-menu" href="#left-menu">
                    <button type="button" class="navbar-toggle collapsed">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </a>
            </div>
            <div class="collapse navbar-collapse" id="">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="/">Главная</a>
                    </li>
                    <li>
                        <a href="/#langauges">Языки</a>
                    </li>
                    <li>
                        <a href="<?= \Scandinaver\Classes\Options::$_forum?>">Форум</a>
                    </li>
                  <!--  <li>
                        <a href="/downloads">Материалы</a>
                    </li> -->
                    <li>
                        <a href="/blog">Блог</a>
                    </li>
                    <?php if (!User::$auth): ?>
                        <li><a href="#login" class="fancybox">Вход</a></li>
                        <li><a href="#registration" class="fancybox">Регистрация</a></li>
                    <?php else: ?>
                        <li><a href="/cabinet"><?= User::$login ?></a></li>
                        <li class="avatar-wrapper-small">
                            <div class="avatar" style="background-image: url(<?= User::$avatar; ?>)"></div>
                        </li>
                        <li><a href="/logout">Выход</a></li>
                    <?php endif; ?>
                    <?php if (User::$_admin): ?>
                        <li><a href="/admin">adminpanel</a></li>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </nav>
    <?php $this->getTemplate(); ?>
    <?php if (User::$_admin) echo(Registry::get('renderer')->render()) ?>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <ul class="list-inline">
                    <li>
                        <a href="#home">Instagram</a>
                    </li>
                    <li class="footer-menu-divider">&sdot;</li>
                    <li>
                        <a href="#about">VK</a>
                    </li>
                    <li class="footer-menu-divider">&sdot;</li>
                    <li>
                        <a href="#feedback">twitter</a>
                    </li>
                    <li class="footer-menu-divider">&sdot;</li>
                    <li>
                        <a href="#feedback" class="fancybox">Письмо администрации</a>
                    </li>
                    <li class="footer-menu-divider">&sdot;</li>
                </ul>
                <p class="copyright text-muted small">Copyright &copy; Scandinaver <?= \Carbon\Carbon::now()->year?>. All Rights Reserved</p>
            </div>
        </div>
    </div>
</footer>
</div>
<div id="registration" class="well bs-component" style="display: none;">
    <form id="registrationform" class="form-horizontal">
        <fieldset>
            <legend class="text-center">Привет!</legend>
            <a class="closeModal" data-dismiss="modal">×</a>
            <div class="form-group">
                <div class="alert alert-dismissible alert-danger hidden" id="badLoginMess">
                    <button type="button" class="hide">×</button>
                    <p></p>
                </div>
                <label for="reg-login" class="col-md-2 col-sm-2 col-xs-2 control-label">Login</label>

                <div class="col-md-10 col-sm-10 col-xs-10">
                    <input id="reg-login" class="form-control" type="text" required/>
                </div>
                <i data-for="reg-login" class="ion-android-done-all text-success"></i>
                <i data-for="reg-login" class="ion-android-warning text-danger"></i>
            </div>
            <div class="form-group">
                <div class="alert alert-dismissible alert-danger hidden" id="badEmailMess">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <p></p>
                </div>
                <label for="reg-email" class="col-md-2 col-sm-2 col-xs-2 control-label">Email</label>
                <div class="col-md-10 col-sm-10 col-xs-10">
                    <input id="fake_reg-email" class="form-control hidden" type="email"/>
                    <input id="reg-email" readonly onfocus="this.removeAttribute('readonly')" class="form-control"
                           type="email" required autocomplete="off"/>
                </div>
                <i data-for="reg-email" class="ion-android-done-all text-success"></i>
                <i data-for="reg-email" class="ion-android-warning text-danger"></i>
            </div>
            <div class="form-group">
                <label for="pass1" class="col-md-2 col-sm-2 col-xs-2 control-label">Пароль</label>
                <div class="col-md-10 col-sm-10 col-xs-10">
                    <input id="fake_pass1" class="form-control hidden" type="password"/>
                    <input id="pass1" class="form-control" type="password" required autocomplete="off"/>
                </div>
                <i data-for="pass1" class="ion-android-done-all text-success"></i>
                <i data-for="pass1" class="ion-android-warning text-danger"></i>
            </div>
            <div class="form-group">
                <div class="alert alert-dismissible alert-danger hidden">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <p>Пароли не совпадают</p>
                </div>
                <label for="pass2" class="col-md-2 col-sm-2 col-xs-2 control-label">Еще раз</label>
                <div class="col-md-10 col-sm-10 col-xs-10">
                    <input id="pass2" class="form-control" type="password" required/>
                </div>
                <i data-for="pass2" class="ion-android-done-all text-success"></i>
                <i data-for="pass2" class="ion-android-warning text-danger"></i>
            </div>
            <div class="form-group text-center">
                <button type="submit" class="btn btn-primary">Регистрация</button>
            </div>
    </form>
</div>
<div id="login" style="display: none;">
    <form id="loginform" action="">
        <a class="closeModal" data-dismiss="modal">×</a>
        <div class="form-group">
            <label for="log" class="col-md-2 control-label">Логин/ email</label>
            <div class="col-md-10">
                <input id="log" class="form-control" type="text" required/>
            </div>
        </div>
        <div class="form-group">
            <label for="pass" class="col-md-2 control-label">Пароль</label>
            <div class="col-md-10">
                <input id="pass" class="form-control" type="password" required/>
            </div>
        </div>
        <div class="form-group text-center">
            <div class="row no-margin">
                <button type="submit" class="btn">Вход</button>
            </div>
            <div class="row no-margin">
                <a id="restore" class="text-muted small text-center" href="#">Восстановить пароль</a>
            </div>
        </div>
    </form>
    <form id="remindform" action="" style="display:none">
        <a class="closeModal" data-dismiss="modal">×</a>
        <div class="form-group">
            <label for="remindlog" class="col-md-2 control-label">Email</label>
            <div class="col-md-10">
                <input id="remindlog" class="form-control" type="email" required/>
            </div>
        </div>
        <div class="form-group text-center">
            <div class="row no-margin">
                <button type="submit" class="btn">Восстановить</button>
            </div>
        </div>
    </form>
</div>
<div id="feedback" style="display: none;">
    <form id="feedbackform" action="">
        <a class="closeModal" data-dismiss="modal">×</a>
        <div class="form-group row">
            <label for="name" class="col-md-2 control-label">Представьтесь:</label>
            <div class="col-md-12">
                <input id="name" class="form-control" type="text" required/>
            </div>
        </div>
       <!-- <div class="form-group row">
            <label for="contact" class="col-md-2 control-label">Ваши контактные данные</label>
            <div class="col-md-10">
                <input id="contact" class="form-control" type="text" required/>
            </div>
        </div>-->
        <div class="form-group row">
            <div class="col-md-12">
                <textarea placeholder="Ваше сообщение" name="message" id="message" cols="50" rows="10"></textarea>
            </div>
        </div>
        <div class="form-group text-center">
            <div class="row no-margin">
                <div class="col-md-12">
                    <button type="submit" class="btn">Отправить</button>
                </div>
            </div>
        </div>
    </form>
</div>
<script>
    $(document).ready(function () {

        $('#left-menu').sidr({
            name: 'sidr-left',
            side: 'left' // By default
        });

        $('a[href="'+window.location.pathname+'"]').closest('li').addClass('active');

        $('a[href="/#langauges"]').on('click', function (e) {
            $('html,body').stop().animate({
                scrollTop: $('#langauges').offset().top
            }, 800);
            e.preventDefault();
        });

        $.material.init();

        $("input#reg-login, input#reg-email").change(function () {
            var role = $(this).attr('id');
            var value = $(this).val();
            $.ajax({
                type: 'post',
                url: '/signup/ajaxCheckRegisterData',
                data: {role: role, value: value},
                dataType: 'json',
                success: function (data) {
                    if (data.success == true) {
                        $('i[data-for="' + role + '"].text-success').show();
                        $('i[data-for="' + role + '"].text-danger').hide();
                    }
                    else {
                        $('i[data-for="' + role + '"].text-success').hide();
                        $('i[data-for="' + role + '"].text-danger').show();
                        $(this).parents('.form-group').addClass('has-error');
                    }
                }
            })
        });
        $('#pass1').on('change', function () {
            $('i[data-for="pass1"].icon-done').show();
        });
        $('#pass2').on('textchange', function () {
            var orig = $('#pass1').val();
            var repeat = $('#pass2').val();
            if (orig == repeat) {
                $('i[data-for=pass2].text-danger').hide();
                $('i[data-for=pass2].text-success').show();
            }
            else {
                $('i[data-for=pass2].text-danger').show();
                $('i[data-for=pass2].text-success').hide();
            }
        });
        $(".fancybox").fancybox({
            padding: 0,
            showNavArrows: false,
            autoDimensions: true,
            centerOnScroll: true,
            hideOnOverlayClick: true,
            overlayOpacity: 1,
            helpers: {
                overlay: {
                    locked: false
                }
            },
            beforeShow: function () {
                $('#main, nav, footer').css({
                    'filter': 'blur(1px)',
                    '-webkit-filter': 'blur(1px)',
                    '-moz-filter': 'blur(1px)',
                    '-o-filter': 'blur(1px)',
                    '-ms-filter': 'blur(1px)'
                });
            },
            afterClose: function () {
                $('#main, nav, footer').css({
                    'filter': 'blur(0px)',
                    '-webkit-filter': 'blur(0px)',
                    '-moz-filter': 'blur(0px)',
                    '-o-filter': 'blur(0px)',
                    '-ms-filter': 'blur(0px)'
                });
            }
        });
        $('.closeModal').on('click', function () {
            $.fancybox.close();
            $('#loginform').show();
            $('#remindform').hide();
        });
        $('#loginform').on('submit', function (e) {
            e.preventDefault();
            var login = $('#log').val();
            var pass = $('#pass').val();
            if (login == '' || pass == '') {
                return false;
            }
            $.ajax({
                url: '/signup/ajaxLogin',
                type: 'post',
                data: {login: login, pass: pass},
                dataType: 'json',
                success: function (data) {
                    if (data.success == true){
                        location.reload()
                    }
                        else{
                        $('#loginform').find('.form-group').addClass('has-error');
                        toastr['error'](data.msg);
                        toastr.options = {
                            "closeButton": true,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toast-bottom-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "10",
                            "hideDuration": "1000",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "linear",
                            "hideEasing": "linear",
                            "showMethod": "slideDown",
                            "hideMethod": "fadeOut"
                        };
                        return false;
                    }
                }
            });
        });
        $('#registrationform').on('submit', function (e) {
            e.preventDefault();
            var email = $('input#reg-email').val();
            var pwdreg = $('input#pass1').val();
            var pwdreg2 = $('input#pass2').val();
            var login = $('input#reg-login').val();
            $('#badLoginMess, #badEmailMess').addClass('hidden');
            if (pwdreg != pwdreg2) {
                toastr['error']('Пароли не совпадают');
                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-bottom-right",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "10",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "linear",
                    "hideEasing": "linear",
                    "showMethod": "slideDown",
                    "hideMethod": "fadeOut"
                };
                return false;
            }
            $.ajax({
                url: '/signup/ajaxRegistration',
                type: 'post',
                data: {email: email, pwdreg: pwdreg, pwdreg2: pwdreg2, login: login},
                dataType: 'json',
                success: function (data) {
                    if (data.success == true) {
                        location.reload()
                    }
                    else {
                        switch (data.code) {
                            case 1:
                                $('#badLoginMess p').text(data.msg);
                                $('#badLoginMess').removeClass('hidden');
                                break;
                            case 2:
                                $('#badEmailMess p').text(data.msg);
                                $('#badEmailMess').removeClass('hidden');
                                break;
                            case 3:
                                $.fancybox.close();
                                toastr[(data.success == true) ? 'success' : 'error'](data.msg);
                                toastr.options = {
                                    "closeButton": true,
                                    "debug": false,
                                    "newestOnTop": false,
                                    "progressBar": false,
                                    "positionClass": "toast-bottom-right",
                                    "preventDuplicates": false,
                                    "onclick": null,
                                    "showDuration": "10",
                                    "hideDuration": "1000",
                                    "timeOut": "5000",
                                    "extendedTimeOut": "1000",
                                    "showEasing": "linear",
                                    "hideEasing": "linear",
                                    "showMethod": "slideDown",
                                    "hideMethod": "fadeOut"
                                };
                                break;
                        }
                    }
                }
            })
        });
        $('#restore').on('click', function () {
            $('#loginform').hide();
            $('#remindform').show();
        });
        $('#remindform').on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                url: '/remind',
                type: 'post',
                data: {email: $('#remindlog').val()},
                dataType: 'json',
                success: function (data) {
                    toastr[(data.success == true) ? 'success' : 'error'](data.msg);
                    toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-bottom-right",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "10",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "linear",
                        "hideEasing": "linear",
                        "showMethod": "slideDown",
                        "hideMethod": "fadeOut"
                    };
                    $.fancybox.close();
                }
            })
        });
        $('#feedbackform').on('submit', function(e){

            e.preventDefault();

            var name = $('#name').val();
           /* var contact = $('#contact').val();*/
            var message = $('#message').val();

            $.ajax({
                url: '/ajaxFeedback',
                type: 'post',
                data: {name: name,/* contact:contact,*/ message:message},
                dataType: 'json',
                success: function(data){
                    toastr[(data.success == true) ? 'success' : 'error'](data.msg);
                    toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-bottom-right",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "10",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "linear",
                        "hideEasing": "linear",
                        "showMethod": "slideDown",
                        "hideMethod": "fadeOut"
                    };
                    $.fancybox.close();
                }
            })
        })
    });
</script>
</body>
</html>
