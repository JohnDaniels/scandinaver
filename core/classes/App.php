<?php

namespace Scandinaver\Classes;

use AltoRouter;
use Carbon\Carbon;
use DebugBar\StandardDebugBar;
use Illuminate\Database\Capsule\Manager;
use Illuminate\Database\Eloquent\Model;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Illuminate\Database\Capsule\Manager as Capsule;
use Symfony\Component\HttpFoundation\Session\Session;

class App{

    /**
     * @var string
     */
    public $Name = 'index()';

    /**
     * @var string
     */
    public static  $moduleName = 'public';

    /**
     * @var Session
     */
    public static $session;
    /**
     * @var string
     */
    public $ControllerName = 'IndexController';

    /**
     * @var string
     */
    public $controller = 'index';

    /**
     * @var string
     */
    public $args = '';

    /**
     * @var EventDispatcher
     */
    public static $dispatcher;

    /**
     * @var User
     */
    public static $user;

    public static $test = 'sss';

	function __construct()
    {
        $this->init();
	}

    private function init()
    {
        define("DS", DIRECTORY_SEPARATOR);
        define('HOST', $_SERVER['HTTP_HOST']);
        define('BASE_URL',realpath(__DIR__."..".DS."..".DS."..".DS));
        define('APPLICATION_PATH', BASE_URL.'/application');
        define('CORE_PATH', BASE_URL.'/core/');
        define('PUBLIC_PATH', BASE_URL.'/public');
    }

	public function go()
    {
        Carbon::setLocale('ru');

        // ide helper
        require_once( __DIR__. '/../idehelper/index.php' );

        /* start events service */
        $listener = new Listeners();
        self::$dispatcher = new EventDispatcher();
        self::$dispatcher->addListener('test.event',         array($listener, 'onTestAction'));
        self::$dispatcher->addListener('registration.event', array($listener, 'onRegistrationAction'));
        self::$dispatcher->addListener('message.event',      array($listener, 'onNewMessageAction'));
        self::$dispatcher->addListener('user.delete',  array($listener, 'onUserDelete'));

        /* create session*/
        self::$session = new \Symfony\Component\HttpFoundation\Session\Session();
        self::$session->start();

        /* initialize config*/
        Options::init();
        $this->connect();
        Options::loadConfig();


        /* create debugbar */
        $debugbar = new StandardDebugBar();
        $debugbar->addCollector(new PHPDebugBarEloquentCollector());
        $debugbarRenderer = $debugbar->getJavascriptRenderer('/js/libs/debugbar');
        Registry::set('debugbar', $debugbar);
        Registry::set('renderer', $debugbarRenderer);

        self::$user = new User();

        /*route request*/
        $url = substr($_SERVER['REQUEST_URI'], 1);
        $parts = explode('/', $url);

        if($parts[0] == 'admin' || $parts[0] == 'vueadmin')
        {
            self::$moduleName = 'admin';
            define('CONTROLLER_PATH',  APPLICATION_PATH.'/controllers/admin/');
            define('TEMPLATE_PATH', __DIR__. '/../../application/view/admin/');
        }
        else{
            define('CONTROLLER_PATH', APPLICATION_PATH.'/controllers/');
            define('TEMPLATE_PATH',  __DIR__. '/../../application/view/public/');
        }

        $router = new AltoRouter();

        $router->addRoutes( include  __DIR__. '/../configuration/routes.php' );
        $match = $router->match();

        if($match){
            $controller = isset($match['target'][0]) ? $match['target'][0]: 'Application\Controllers\IndexController';
            $action = isset($match['target'][1]) ? $match['target'][1]: 'IndexAction';
        }
        else{
            $controller =  'Application\Controllers\IndexController';
            $action = 'pageNotFound';
            $match['params'] = [];
        }

        $controller = new $controller;

        if(!method_exists($controller, $action))
            $action = 'pageNotFound';

        /*start*/
        call_user_func_array([$controller, $action], $match['params']);
	}

    private function connect()
    {
        $capsule = new Capsule;

        $capsule->addConnection([
            'driver'    => 'mysql',
            'host'      => 'localhost',
            'database'  => Options::$dbase,
            'username'  => Options::$dbuser,
            'password'  => Options::$dbpass,
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        ]);

        $capsule->setAsGlobal();

        $capsule->bootEloquent();

        Registry::set('capsule', $capsule);
    }

    static function goHome($way = '')
    {
        header('Location: http://'.HOST.'/'.$way);
    }
}