<?php
header($_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found');
header('Content-type: text/html; charset=UTF-8');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">
<head>
    <title>404 Not Found</title>
    <link href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
</head>
<body style="background: #505D6E url('/img/404.jpg') no-repeat fixed center center / cover;">
<!-- Контент  -->
<div class="content" style="text-align: center">
    <h1 style="font-family: 'Raleway',sans-serif;font-size: 200px;text-align: center;margin-bottom: 0">404</h1>
    <a href="/">На главную</a>
</div>

</body>
</html>