var gulp = require('gulp');
var mainBowerFiles = require('main-bower-files');
var csso = require('gulp-csso'); // Минификация CSS
var concat =  require('gulp-concat');
var cssmin = require('gulp-cssmin');
var rename = require('gulp-rename');
var runSequence = require('run-sequence');
var uglify = require('gulp-uglify');

gulp.task('mainfiles', function() {
    return gulp.src(mainBowerFiles())
        .pipe(gulp.dest('application/assets/dist'))
});

gulp.task('concat', function() {
     return gulp.src(['application/assets/css/bootstrap.css',
                     'application/assets/css/bootstrap-material-design.css',
                     'application/assets/css/ripples.min.css',
                     'application/assets/js/lib/toastr/toastr.min.css',
                     'application/assets/fonts/ionicons/css/ionicons.min.css',
                     'application/assets/css/jquery.sidr.light.css',
                     'application/assets/css/landing-page.css'])
         .pipe(concat('style.css'))
         .pipe(gulp.dest('public/css/'));
});

gulp.task('js', function(){
    return gulp.src(['application/assets/js/jquery-1.11.21.min.js',
                     //'application/assets/js/scroll.js',
                     'application/assets/js/ripples.min.js',
                     'application/assets/js/lib/toastr/toastr.min.js',
                     'application/assets/js/bootstrap.min.js',
                     'application/assets/js/textChange.js',
                     'application/assets/js/material.js',
                     'application/assets/js/lib/fancybox/jquery.fancybox.pack.js',
                     'application/assets/js/lib/jquery.sidr.min.js',
                     'application/assets/js/parallax.min.js',
                     'application/assets/js/lib/boxLoader/js/jquery.boxloader.min.js'])
        .pipe(concat('script.js'))
        .pipe(uglify())
        .pipe(gulp.dest('public/js/'));
});

gulp.task('minify', ['concat'], function(){
    gulp.src('public/css/style.css')
        .pipe(csso())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('public/css/'));
});

gulp.task('uglify', function(){
    gulp.src('public/*.js')
        .pipe(uglify())
        .pipe(gulp.dest('application/assets/build/'))
});



gulp.task('styles', function() {
    runSequence('concat', 'minify');
});

gulp.task('watch', function (){
    gulp.watch('application/assets/css/*.css', ['concat', 'minify']);
});
