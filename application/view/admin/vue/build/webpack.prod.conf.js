'use strict'

const path = require('path')
const webpack = require('webpack')

const ExtractTextPlugin = require('extract-text-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')

const projectRoot = path.resolve(__dirname, '../')

const webpackConfig = {

    entry: './application/view/admin/vue/client/index.js',

    output: {
        path: path.resolve('public/build'),
        publicPath: 'build/',
        filename: 'admin.js',
        chunkFilename: 'assets/js/[id].[chunkhash].js'
    },

    module: {
        loaders: [
        //    {
        //        test: /\.(js|vue)$/,
        //        loader: 'eslint-loader',
        //        //include: projectRoot,
        //        exclude: /node_modules/,
        //        enforce: 'pre',
        //        options: {
        //            formatter: require('eslint-friendly-formatter')
        //        }
        //    },
            {
                test: /\.vue$/,
                loader: 'vue-loader',
                options: require('./vue-loader.conf')
            },
            {
                test: /\.js$/,
                loader: 'babel-loader',
                //include: projectRoot,
                exclude: /node_modules/
            },
            {
                test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
                loader: 'url-loader',
                query: {
                    limit: 10000,
                    name: 'assets/img/[name].[hash:7].[ext]'
                }
            },
            {
                test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
                loader: 'url-loader',
                query: {
                    limit: 10000,
                    name: 'assets/fonts/[name].[ext]'
                }
            },
            {
                test: /\.scss$/,
                loaders: [ 'style-loader', 'css-loader', 'sass-loader' ]
            },
            {
                test: /\.css$/,
                loaders: ['style-loader', 'css-loader' ]
            }
        ]
    },

    resolve: {
        extensions: ['.js', '.vue', '.css', '.json'],
        alias: {
            package: path.resolve('application/view/admin/vue/package.json'),
            src: path.resolve(__dirname, '../client'),
            assets: path.resolve(__dirname, '../client/assets'),
            components: path.resolve(__dirname, '../client/components'),
            views: path.resolve(__dirname, '../client/views'),
            'plotly.js': 'plotly.js/dist/plotly',
            'vuex-store': path.resolve(__dirname, '../client/store'),
            vue: 'vue/dist/vue.js'
        }
    },

    plugins: [

        new webpack.LoaderOptionsPlugin({ minimize: true }),

        new ExtractTextPlugin('assets/css/style.css'),

        new webpack.ProvidePlugin({
            'window.Quill': 'quill/dist/quill.js',
            'Quill': 'quill/dist/quill.js',
        }),

        //new HtmlWebpackPlugin({
        //    title: 'Admin',
        //    filename:  path.resolve(__dirname, '../index.html'),
        //    template: 'application/view/admin/vue/indextmpl.html',
        //    inject: true,
        //    favicon: 'application/view/admin/vue/client/assets/logo.png',
        //    minify: {
        //        removeComments: false,
        //        collapseWhitespace: false,
        //        removeAttributeQuotes: false
        //    }
        //}),
    ],
    // See https://github.com/webpack/webpack/issues/3486
    performance: {
        hints: false
    }
}

module.exports = webpackConfig
