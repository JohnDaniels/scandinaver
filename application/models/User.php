<?php

namespace Application\Models;

use Eloquent;
use Scandinaver\Classes\App;
use Scandinaver\Events\GenericEvent;


/**
 * Class UsersModel
 *
 * @property int $id
 * @property string $login
 * @property string $name
 * @property string $pass
 * @property string $photo
 * @property string $avatar
 * @property string $restore_link
 * @property string $email
 * @property int $active
 * @property string $role
 * @property int $created_at
 * @property int $last_online
 * @property string $openpass
 * @property int $active_to
 */
class User extends Eloquent
{

    protected $table = 'users';

    protected $fillable = ['name', 'login', 'email', 'pass', 'openpass', 'role'];

    protected $hidden = ['openpass', 'pass'];

    protected $dates = ['active_to'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->hasMany('Application\Models\Comment');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sessions()
    {
        return $this->hasMany('Application\Models\Session');
    }


    public function delete()
    {
        App::$dispatcher->dispatch('user.delete', new GenericEvent($this));

        return parent::delete();
    }

    /**
     * При создании юзера сразу задаем ему basic assets!
     * @param $userParams
     * @return bool
     */
    public function addUser($userParams)
    {
        $params = array(
            'login' => $userParams['login'],
            'email' => $userParams['email'],
            'pass' => md5($userParams['pass']),
        );

        $st = $this->DB->prepare('
                INSERT INTO `users`
                (`login`, `email`, `pass`)
                VALUES
                (:login, :email, :pass)
            ');
        $st->execute($params);

        $last_user_id = $this->DB->lastInsertId();

        $st = $this->DB->prepare('
                            INSERT INTO assets_to_users
                            (asset_id, user_id)
                            VALUES
                            (1, 1)

                            ');
        // $st->bindParam('last_user_id',$last_user_id );
        $st->execute();
    }

    public function saveRestoreHash($id, $hash)
    {
        $st = $this->DB->prepare('
                UPDATE users
                SET restore_link = :hash
                WHERE id = :id');

        $st->bindParam('hash', $hash);
        $st->bindParam('id', $id);

        if ($st->execute())
            return true;
    }

    public function updatePassword($id, $pass)
    {
        $openpass = $pass;
        $pass = md5($pass);

        $st = $this->DB->prepare('
                        UPDATE users
                        SET pass = :pass,
                            openpass = :openpass,
                            restore_link = ""
                        WHERE id = :id');
        $st->bindParam('pass', $pass);
        $st->bindParam('openpass', $openpass);
        $st->bindParam('id', $id);
        return $st->execute();
    }

    public function checkRestoreLink($id, $link)
    {
        $q = $this->DB->prepare('
                        SELECT *
                        FROM users
                        WHERE id = :id
                          AND restore_link = :link');

        $q->bindParam('id', $id);
        $q->bindParam('link', $link);
        $q->execute();
        return $q->fetchAll(PDO::FETCH_ASSOC);
    }

    /**Авторизация приложения и поддоменов. Просто проверяем юзера
     * @param $login
     * @param $pass
     * @return bool
     */
    public function autorizeApp($login, $pass)
    {
        $r = $this->getUser($login, md5($pass));

        if ($r) {
            l('autorized by app login: ' . $login . ' pass: ' . $pass);
            return $r;
        } else {
            l('autorized by app failed: ' . $login . ' pass: ' . $pass);
            return false;
        }
    }


    public function checkUser($email)
    {
        $st = $this->DB->prepare('SELECT * FROM users WHERE email = :email');

        $st->bindParam('email', $email);
        $st->execute();
        $r = $st->fetch(PDO::FETCH_OBJ);

        if (count($r) > 0) {

            return $r;
        } else {
            return false;
        }
    }

    /**
     * @return bool|string
     */
    public function generateLink()
    {
        $link = 'http://www.' . HOST . '/restore/' . self::$id . '/';
        $hash = sha1(time() . self::$email);
        $link .= $hash;

        $st = $this->DB->prepare('
                UPDATE users
                SET restore_link = :hash
                WHERE id = :id');

        $st->bindParam('hash', $hash);
        $st->bindParam('id', self::$id);

        if ($st->execute())
            return $link;
        else
            return false;
    }

    public function getUser($login, $password)
    {
        $q = $this->DB->prepare('
                SELECT * FROM users
                WHERE (email = :email OR login = :login)
                AND pass = :pass
                ');
        $q->bindParam('email', $login);
        $q->bindParam('login', $login);
        $q->bindParam('pass', $password);
        $q->execute();

        if ($r = $q->fetch(PDO::FETCH_ASSOC))
            return $r;
        else
            return false;
    }
}