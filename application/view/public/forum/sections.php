<?php
use Scandinaver\Classes\User;
?>

<section id="main" class="container" style="margin-top: 50px;margin-bottom: 50px;">
    <?php foreach ($this->data->sections as $section):?>
        <div class="row">
            <h3><?= $section->name ?></h3>
        </div>
        <div class="row forum-section">
            <header class="col-md-12">
                <div class="row forum-header">
                    <div class="col-lg-8 col-md-8 col-sm-7 col-xs-5 text-left">Форум</div>
                    <div class="col-lg-4 col-md-4 col-sm-5 col-xs-7">Последнее сообщение</div>
                </div>
            </header>
            <div class="col-md-12">
                <div class="row forums-wrapper">
                    <?php foreach ($section->forums as $forum): ?>
                        <?php if(!$forum->active && !User::$_admin) continue;?>
                        <div class="row forum">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="forum-name">
                                            <i class="<?= $forum->icon ?>"></i>
                                          <a class="data" href="/forum/<?= $forum->id ?>">
                                                <?= $forum->name ?> <?= (!$forum->active && User::$_admin)? '<sup>не активен</sup>' : ''?>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="row forum-stats">
                                    <div class="col-lg-10 col-md-10">
                                        <div class="row forum-stats small no-margin">
                                            <div class="col-md-12">
                                                Тем: <span><?= $forum->topics->count() ?></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-5 col-md-5 col-sm-4 hidden-xs">
                                <p class="pull-left data"><?= $forum->description ?></p>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-5 col-xs-7">
                                <?php if($forum->topics->count()):?>
                                <p><i class="ion-ios-redo-outline"></i>
                                    <a class="last-topic-link"
                                       href="/forum/<?= $forum->id ?>/<?= $forum->topics[0]->id ?>?page=<?= ceil($forum->topics[0]->posts->count() / \Scandinaver\Classes\Options::$config->forum_posts_on_page) ?>">
                                        <?= $forum->topics[0]->name ?>
                                    </a>
                                </p>
                                <?php endif;?>
                                <?php if ($forum->topics[0]->posts[0]): //если в теме есть сообщения выводим дату и автора?>
                                    <p class="no-margin small">
                                        <span class="pull-right"><?= $forum->topics[0]->posts[0]->created_at->format('Y.m.d H:m') ?></span>
                                        <span class="text-red"> Автор: </span><?= $forum->topics[0]->posts[0]->author->name ?>
                                    </p>
                                <?php endif ?>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</section>