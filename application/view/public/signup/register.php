<!-- Header -->
<header id="header">
    <h1><a href="../../../../public/index.php">Alpha</a> by HTML5 UP</h1>
    <nav id="nav">
        <ul>
            <li><a href="/">Home</a></li>
            <li><a href="/signup/">Регистрация</a></li>
            <li><a href="/forum/">Форум</a></li>
        </ul>
    </nav>
</header>

<section id="main" class="container 75%" style="margin-top:0;">
    <header>
        <h2>Contact Us</h2>
        <p>Tell us what you think about our little operation.</p>
    </header>
    <div class="box">
        <form method="post" action="">
            <div class="row uniform 50%">
                <div class="6u 12u(mobilep)">
                    <input type="text" name="login" id="login" value="" placeholder="Login" />
                </div>
                <div class="6u 12u(mobilep)">
                    <input type="text" name="email" id="email" value="" placeholder="Email" />
                </div>
                <div class="6u 12u(mobilep)">
                    <input type="password" name="pwdreg" id="pwdreg" value="" placeholder="Password" />
                </div>
            </div>
            <div class="row uniform">
                <div class="12u">
                    <ul class="actions align-center">
                        <li><input name="registration" type="submit" value="Регистрация" /></li>
                    </ul>
                </div>
            </div>
        </form>
    </div>
</section>
