<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 04.03.2016
 * Time: 13:01
 * *                    // Match all request URIs
 *   [i]                  // Match an integer
 *   [i:id]               // Match an integer as 'id'
 *   [a:action]           // Match alphanumeric characters as 'action'
 *   [h:key]              // Match hexadecimal characters as 'key'
 *   [:action]            // Match anything up to the next / or end of the URI as 'action'
 *   [create|edit:action] // Match either 'create' or 'edit' as 'action'
 *   [*]                  // Catch all (lazy, stops at the next trailing slash)
 *   [*:trailing]         // Catch all as 'trailing' (lazy)
 *   [**:trailing]        // Catch all (possessive - will match the rest of the URI)
 *   .[:format]?          // Match an optional parameter 'format' - a / or . before the block is also optional
 *
 *   map users details page using controller#action string
 *   $router->map( 'GET', '/users/[i:id]/', 'UserController#showDetails' );
 *
 *   array('PATCH','/users/[i:id]', 'users#update', 'update_user'),
 */

return [

    ['GET|POST',  '/forum/uploadImage',    ['Application\Controllers\\'.'ForumController', 'uploadImage']],

    //*****************************API******************************************//

    ['GET', '/api/login/[:login]/[:password]',    ['Application\Controllers\\'.'ApiController', 'login']],
    ['GET', '/api/assets/[a:lang]/[i:uid]',        ['Application\Controllers\\'.'ApiController', 'assets']],

    ['GET', '/api/users',                         ['Application\Controllers\\'.'ApiController', 'users']],
    ['GET', '/api/setSession/[i:uid]/[a:token]',  ['Application\Controllers\\'.'ApiController', 'setSession']],

    //*****************************PUBLIC******************************************//

    ['GET',   '/',                                ['Application\Controllers\\' . 'IndexController', 'index']],
    ['POST',   '/signup/ajaxLogin',                ['Application\Controllers\\'.'SignupController', 'ajaxLogin']],
    ['POST',   '/signup/ajaxRegistration',         ['Application\Controllers\\'.'SignupController', 'ajaxRegistration']],
    ['POST',   '/signup/ajaxCheckRegisterData',    ['Application\Controllers\\'.'SignupController', 'ajaxCheckRegisterData']],
    ['POST',   '/ajaxRegistration',                ['Application\Controllers\\'.'SignupController', 'ajaxRegistration']],
    ['POST',   '/ajaxFeedback',                    ['Application\Controllers\\'.'IndexController', 'ajaxFeedback']],

    ['GET',   '/logout',                          ['Application\Controllers\\' . 'LogoutController', 'logout']],

    ['GET|POST', '/blog/[i:post_id]',                  ['Application\Controllers\\'.'BlogController', 'post']],
    ['GET', '/blog',                              ['Application\Controllers\\'.'BlogController', 'index']],

    ['POST', '/cabinet/uploadImage',              ['Application\Controllers\\'.'ProfileController', 'uploadImage']],
    ['POST', '/cabinet/update',                    ['Application\Controllers\\'.'ProfileController', 'update']],
    ['GET', '/cabinet',                           ['Application\Controllers\\'.'ProfileController', 'index']],

    ['GET', '/restore/[i:id]/[a:link]',           ['Application\Controllers\\'.'RemindController', 'restore']],
    ['GET', '/remind/remind',                     ['Application\Controllers\\'.'RemindController', 'remind']],
    ['GET', '/remind',                            ['Application\Controllers\\'.'RemindController', 'index']],


    //*****************************ADMIN******************************************//

    ['GET',        '/admin/logout',               ['Application\Controllers\Admin\\' . 'VueController', 'logout']],
    //['GET|POST',   '/admin',                      ['Application\Controllers\Admin\\' . 'IndexController', 'index']],

    ['GET',  '/admin/messages/get/[i:id]',        ['Application\Controllers\Admin\\'.'IndexController', 'ajaxGetMessage']],

    ['GET|POST',  '/admin/seo/[i:id]',                 ['Application\Controllers\Admin\\'.'SeoController', 'edit']],
    ['DELETE',  '/admin/seo/delete',             ['Application\Controllers\Admin\\'.'SeoController', 'delete']],
    ['POST',  '/admin/seo/add',                ['Application\Controllers\Admin\\'.'SeoController', 'add']],
    ['GET',  '/admin/seo',                        ['Application\Controllers\Admin\\'.'SeoController', 'index']],

    //*****************************VUEADMIN******************************************//
    ['GET|POST',   '/admin',                      ['Application\Controllers\Admin\\' . 'VueController', 'index']],

    ['GET',       '/admin/dashboard',            ['Application\Controllers\Admin\\' . 'VueController', 'dashboard']],
    ['DELETE',    '/admin/log/[i:id]',           ['Application\Controllers\Admin\\' . 'VueController', 'deleteLog']],
    ['DELETE',    '/admin/message/[i:id]',       ['Application\Controllers\Admin\\' . 'VueController', 'deleteMessage']],
    ['POST',      '/admin/message/read/[i:id]',  ['Application\Controllers\Admin\\' . 'VueController', 'readMessage']],
    ['GET',       '/admin/dashboard',            ['Application\Controllers\Admin\\' . 'VueController', 'dashboard']],

    ['GET',  '/users',                           ['Application\Controllers\Admin\\'.  'UsersController', 'index']],
    ['GET',  '/user/[i:id]',                     ['Application\Controllers\Admin\\'.  'UsersController', 'user']],
    ['GET',  '/user/search',                     ['Application\Controllers\Admin\\'.  'UsersController', 'search']],
    ['POST', '/admin/users/active',              ['Application\Controllers\Admin\\' . 'UsersController', 'active']],
    ['DELETE',  '/user/[i:id]',                  ['Application\Controllers\Admin\\'.'UsersController', 'delete']],

    ['GET',  '/articles',                        ['Application\Controllers\Admin\\'.  'ArticleController', 'index']],
    ['GET',  '/article/[i:id]',                  ['Application\Controllers\Admin\\'.  'ArticleController', 'article']],

    ['GET',   '/categories',                      ['Application\Controllers\Admin\\'.'CategoryController', 'categories']],
    ['DELETE','/category/[i:id]',                 ['Application\Controllers\Admin\\'.'CategoryController', 'delete']],
    ['POST',  '/category/[i:id]',                 ['Application\Controllers\Admin\\'.'CategoryController', 'edit']],
    ['POST',  '/category',                        ['Application\Controllers\Admin\\'.'CategoryController', 'add']],

    ['DELETE','/article/[i:id]',                 ['Application\Controllers\Admin\\'.'ArticleController', 'delete']],
    ['GET',  '/article/search',                  ['Application\Controllers\Admin\\'.'ArticleController', 'search']],
    ['POST',  '/admin/upload',                   ['Application\Controllers\Admin\\'.'ArticleController', 'upload']],
    ['POST',  '/article/[i:id]',                 ['Application\Controllers\Admin\\'.'ArticleController', 'edit']],
    ['POST',  '/article',                        ['Application\Controllers\Admin\\'.'ArticleController', 'add']],

    ['GET',   '/comments',                       ['Application\Controllers\Admin\\'.'CommentController', 'comments']],
    ['GET',   '/comments/search',                ['Application\Controllers\Admin\\'.'CommentController', 'search']],
    ['DELETE','/comment/[i:id]',                 ['Application\Controllers\Admin\\'.'CommentController', 'delete']],

    ['GET',   '/seo',                            ['Application\Controllers\Admin\\'.'SeoController', 'index']],
    ['POST',  '/seo',                            ['Application\Controllers\Admin\\'.'SeoController', 'add']],
    ['POST',  '/seo/[i:id]',                     ['Application\Controllers\Admin\\'.'SeoController', 'edit']],
    ['DELETE','/seo/[i:id]',                     ['Application\Controllers\Admin\\'.'SeoController', 'delete']],
];