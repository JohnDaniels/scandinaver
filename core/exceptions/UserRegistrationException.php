<?php
namespace Scandinaver\Exceptions;
use Exception;

/**
 * Created by PhpStorm.
 * User: user
 * Date: 22.03.2016
 * Time: 23:41
 */

class UserRegistrationException extends Exception{

    protected $login;
    protected $pass;

    public function __construct($msg, $login = '', $pass = '', $code = 0)
    {
        parent::__construct($msg, $code);
        
        $this->login = $login;
        $this->pass = $pass;
    }

    public function getLogin()
    {
        return $this->login;
    }

    public function getPass()
    {
        return $this->pass;
    }
}