<?php
use Scandinaver\Classes\Options;
use Scandinaver\Classes\Registry;
?>
<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
    <title>Icelandic</title>

    <link rel="stylesheet" href="/css/style.min.css"  media="screen">

    <script src="/js/script.min.js"></script>
    <script src="/js/lib/bootstrap.min.js"></script>

    <script src="/js/lib/fancybox/jquery.fancybox.pack.js"></script>

    <link href="/js/lib/fancybox/jquery.fancybox.css" rel="stylesheet">
    <link href="/css/bootstrap.css" rel="stylesheet">
    <link href="/css/bootstrap-theme.css" rel="stylesheet">

    <link href="/css/admin.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <?php // echo(Registry::get('renderer')->renderHead())?>
</head>
<body class="background-dark">
<nav id="sidr-left" style="left: -260px;position: fixed;">
    <ul>
        <li><a href="/admin/">Dashboard</a></li>
        <li><a href="/admin/users/">Юзеры</a></li>
        <li><a href="/admin/assets/">Наборы</a></li>
        <li><a href="/admin/dict/">Словарь</a></li>
        <li><a href="/admin/text/">Тексты</a></li>
        <li><a href="/admin/config/">Настройки</a></li>
        <li><a href="/">icelandic</a></li>
        <li><a href="/logout/">Exit</a></li>
    </ul>
</nav>
<div class="wrap" id="panel">
    <div class="navbar navbar-static-top navbar-fixed-left" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <a id="left-menu" href="#left-menu">
                    <button type="button" class="navbar-toggle collapsed">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </a>
            </div>
            <div class="head-full-menu hidden-xs">
                <ul class="nav navbar-nav" id="main-menu">
                    <li><a href="/" target="_blank"><?=Options::$site?></a></li>
                    <li><a href="/admin">Dashboard</a></li>
                    <li><a href="/admin/users">Юзеры</a></li>
                    <li><a href="/admin/assets">Наборы</a></li>
                    <li><a href="/admin/dict">Словарь</a></li>
                    <li><a href="/admin/text">Тексты</a></li>
                    <li><a href="/admin/config">Настройки</a></li>
                    <li><a href="/admin/intro">intro.js</a></li>
                    <li><a href="/logout">Exit</a></li>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </div>
    <div class="container-fluid">
        <!--getTemplate-->
        <?php $this->getTemplate();?>
        <!--end getTemplate-->
    </div>
</div>

<?php //echo(Registry::get('renderer')->render())?>
    </body>
<script>
    $(function(){
        $('#main-menu a[href = "'+window.location.pathname+'"]').parents('li').addClass('active')
    })
</script>
</html>